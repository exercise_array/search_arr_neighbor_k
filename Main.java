public class Main {

    /**
     * A step array is an array of integer where each element has a difference of atmost k with its neighbor.
     * Given a key x, we need to find the index value of k if multiple element exist return the first occurrence of key.
     */
    public static void main(String[] args) {
        int arr[] = {1, 3, 4, 6, 7, 9};
        int x = 6;
        int k = 2;
        int n = arr.length;

        int pos = search(arr, n, x, k);
        if (pos == -1) {
            System.out.println("Khong tim thay");
        } else {
            System.out.println("Phan tu tim thay tai: " + pos);
        }
    }

    static int search(int arr[], int n, int x, int k) {

        int i = 0;

        while (i < n) {

            if (arr[i] == x)
                return i;
            i = i + Math.max(1, Math.abs(arr[i]
                    - x) / k);
        }
        return -1;
    }
}
